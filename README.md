This program is used for control light in buildings and on street in lego city. App send data through HTTP request in JSON format. User can control lights by click correct bulb and switch floors through vertical swipe. <br />

Frontend: QML <br />
Backend: C++

![Scheme](images/firstFloor.png)
![Scheme](images/secondFloor.png)
