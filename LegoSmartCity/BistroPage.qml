import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: page
    width: 800
    height: 600

    property var bits: [13, 1, 14, 8, 10, 0, 3, 4, 12]
    property int clickedIndex: 0
    property bool clickedState: false

    header: Label {
        text: qsTr("Bistro")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Image {
        id: image
        anchors.left: parent.left
        anchors.right: frame2.left
        anchors.top: parent.top
        anchors.bottom: frame1.top

        source: "images/8.png"
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 0
        autoTransform: false
        fillMode: Image.PreserveAspectFit
    }

    Frame {
        id: frame1
        y: 470
        height: 60
        anchors.left: parent.left
        anchors.right: frame2.left
        anchors.bottom: parent.bottom
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.leftMargin: 20

        Row {
            id: row
            anchors.fill: parent
            spacing: 5

            Repeater{
                id: buttons1
                model: ["L1", "L2", "L3"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true

                    onClicked: {
                        clickedIndex = index
                        clickedState = !clickedState
                    }
                }
            }
        }
    }

    Frame {
        id: frame2
        x: 653
        width: 125
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 0

        Column {
            id: column
            anchors.fill: parent

            Repeater{
                id: buttons2
                model: ["L4", "L5", "L6", "L7", "L8", "L9"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true

                    onClicked: {
                        clickedIndex = index+3
                        clickedState = !clickedState
                    }
                }
            }
        }
    }

    function checkButton(index, state){
        if(index < 3)
            buttons1.itemAt(index).checked = state
        else
            buttons2.itemAt(index-3).checked = state
    }

    function buttonState(index){
        if(index < 3)
            return buttons1.itemAt(index).checked
        else
            return buttons2.itemAt(index-3).checked
    }
}
