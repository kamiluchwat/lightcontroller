import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: page
    width: 800
    height: 600

    property var bits: [8, 14, 12, 13, 9, 10, 0, 1, 2, 4, 6]
    property int clickedIndex: 0
    property bool clickedState: false

    header: Label {
        text: qsTr("Warsztat")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Image {
        id: image
        anchors.left: parent.left
        anchors.right: frame2.left
        anchors.top: parent.top
        anchors.bottom: frame1.top

        source: "images/5.png"
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 0
        autoTransform: false
        fillMode: Image.PreserveAspectFit
    }

    Frame {
        id: frame1
        y: 470
        height: 60
        anchors.left: parent.left
        anchors.right: frame2.left
        anchors.bottom: parent.bottom
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.leftMargin: 20

        Row {
            id: row
            anchors.fill: parent
            spacing: 5

            Repeater{
                id: buttons1
                model: ["L1", "L2", "L3", "L4"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true

                    onClicked: {
                        clickedIndex = index
                        clickedState = !clickedState
                    }
                }
            }
        }
    }

    Frame {
        id: frame2
        x: 653
        width: 125
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 0

        Column {
            id: column
            anchors.fill: parent

            Repeater{
                id: buttons2
                model: ["L5", "L6", "L7", "L8", "L9", "L10", "L11"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true

                    onClicked: {
                        clickedIndex = index+4
                        clickedState = !clickedState
                    }
                }
            }
        }
    }

    function checkButton(index, state){
        if(index < 4)
            buttons1.itemAt(index).checked = state
        else
            buttons2.itemAt(index-4).checked = state
    }

    function buttonState(index){
        if(index < 4)
            return buttons1.itemAt(index).checked
        else
            return buttons2.itemAt(index-4).checked
    }
}
