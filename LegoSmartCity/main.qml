import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    width: 800
    minimumWidth: 700
    height: 600
    minimumHeight: 400
    visible: true
    title: qsTr("LegoSmartCity Controller")

    property var status: Array(12).fill(0)

    SwipeView
    {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        BankPage
        {
            id: bankPage

            sceneZeroFloor.onBulbswitchChanged: appcore.onSwitch(0,  bitsZeroFloor[sceneZeroFloor.currentIndex], sceneZeroFloor.bulbState(sceneZeroFloor.currentIndex))
            sceneFirstFloor.onBulbswitchChanged: appcore.onSwitch(0,  bitsFirstFloor[sceneFirstFloor.currentIndex], sceneFirstFloor.bulbState(sceneFirstFloor.currentIndex))
        }

        SquarePage
        {
            id: squarePage

            onClickedStateChanged: {
                if(clickedIndex < 14)
                    appcore.onSwitch(1, bits1[clickedIndex], buttonState(clickedIndex))
                else
                    appcore.onSwitch(2, bits2[clickedIndex-14], buttonState(clickedIndex))
            }
        }

        RestaurantPage
        {
            id: restaurantPage

            onClickedStateChanged: appcore.onSwitch(3, bits[clickedIndex], buttonState(clickedIndex))
        }

        CinemaPage
        {
            id: cinemaPage

            onClickedStateChanged: appcore.onSwitch(4, bits[clickedIndex], buttonState(clickedIndex))
        }

        WorkshopPage
        {
            id: workshopPage

            onClickedStateChanged: appcore.onSwitch(5, bits[clickedIndex], buttonState(clickedIndex))
        }

        PetshopPage
        {
            id: petshopPage

            onClickedStateChanged: appcore.onSwitch(6, bits[clickedIndex], buttonState(clickedIndex))
        }

        DetectiveOfficePage
        {
            id: detectiveOfficePage

            onClickedStateChanged: appcore.onSwitch(7, bits[clickedIndex], buttonState(clickedIndex))
        }

        BistroPage
        {
            id: bistroPage

            onClickedStateChanged: appcore.onSwitch(8, bits[clickedIndex], buttonState(clickedIndex))
        }

        HousePage
        {
            id: housePage

            onClickedStateChanged: appcore.onSwitch(9, bits[clickedIndex], buttonState(clickedIndex))
        }

        CafeteriaPage
        {
            id: cafeteriaPage

            onClickedStateChanged: appcore.onSwitch(9, bits[clickedIndex], buttonState(clickedIndex))
        }

        Station1Page
        {
            id: station1Page

            onClickedStateChanged: appcore.onSwitch(6, bits[clickedIndex], buttonState(clickedIndex))
        }

        Station2Page
        {
            id: station2Page

            onClickedStateChanged: appcore.onSwitch(9, bits[clickedIndex], buttonState(clickedIndex))
        }

        RollercoasterPage
        {
            id: rollercoasterPage

            onClickedStateChanged: appcore.onSwitch(10, bits[clickedIndex], buttonState(clickedIndex))
        }

        CityPage
        {
            id: cityPage

            onClickedStateChanged:{
                if(clickedIndex < 2)
                    appcore.onCreate(clickedIndex)
                else if(clickedIndex == 2)
                    appcore.onSwitch(8, 7, !buttonState(clickedIndex))
                else if(clickedIndex == 3){
                    appcore.onSwitch(0, bits[0], buttonState(clickedIndex))
                    appcore.onSwitch(1, bits[1], buttonState(clickedIndex))
                    appcore.onSwitch(1, bits[2], buttonState(clickedIndex))
                    appcore.onSwitch(3, bits[3], buttonState(clickedIndex))
                    appcore.onSwitch(4, bits[4], buttonState(clickedIndex))
                    appcore.onSwitch(5, bits[5], buttonState(clickedIndex))
                    appcore.onSwitch(6, bits[6], buttonState(clickedIndex))
                    appcore.onSwitch(7, bits[7], buttonState(clickedIndex))
                    appcore.onSwitch(8, bits[8], buttonState(clickedIndex))
                }
                else{
                    if(clickedIndex==6)
                        appcore.onSwitch(1, bits[clickedIndex-4], buttonState(clickedIndex))
                    else
                        appcore.onSwitch(clickedIndex-4, bits[clickedIndex-4], buttonState(clickedIndex))
                }
            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        Repeater{
            id: repeater_tabbar
            model: ["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "D1", "D2", "RC", "MS"]

            TabButton {
                text: modelData
            }
        }
    }

    Connections {
        target: appcore
        function onIsDataChanged()
        {
            for(var i=0 ; i<status.length ; i++){
                status[i] = appcore.getValue(i)
            }

            for(var j=0 ; j<3 ; j++){
                bankPage.sceneZeroFloor.lightSwitch(j, status[0] & (1 << bankPage.bitsZeroFloor[j]))
                bankPage.sceneFirstFloor.lightSwitch(j, status[0] & (1 << bankPage.bitsFirstFloor[j]))

                housePage.checkButton(j, status[9] & (1 << housePage.bits[j]))

                cafeteriaPage.checkButton(j, status[9] & (1 << cafeteriaPage.bits[j]))
            }

            for(var k=0 ; k<5 ; k++){
                cinemaPage.checkButton(k, status[4] & (1 << cinemaPage.bits[k]))
            }

            for(var l=0 ; l<6 ; l++){
                rollercoasterPage.checkButton(l, status[10] & (1 << rollercoasterPage.bits[l]))

                station2Page.checkButton(l, status[9] & (1 << station2Page.bits[l]))
            }

            for(var m=0 ; m<4 ; m++){
                station1Page.checkButton(m, status[6] & (1 << station1Page.bits[m]))
            }

            for(var n=0 ; n<9 ; n++){
                restaurantPage.checkButton(n, status[3] & (1 << restaurantPage.bits[n]))

                bistroPage.checkButton(n, status[8] & (1 << bistroPage.bits[n]))

                squarePage.checkButton(n+14, status[2] & (1 << squarePage.bits2[n]))

                if(n == 2)
                    cityPage.checkButton(n+4, status[1] & (1 << cityPage.bits[n]))
                else
                    cityPage.checkButton(n+4, status[n] & (1 << cityPage.bits[n]))
            }

            for(var o=0 ; o<11 ; o++){
                workshopPage.checkButton(o, status[5] & (1 << workshopPage.bits[o]))
            }

            for(var p=0 ; p<8 ; p++){
                petshopPage.checkButton(p, status[6] & (1 << petshopPage.bits[p]))
            }

            for(var r=0 ; r<10 ; r++){
                detectiveOfficePage.checkButton(r, status[7] & (1 << detectiveOfficePage.bits[r]))
            }

            for(var s=0 ; s<14 ; s++){
                squarePage.checkButton(s, status[1] & (1 << squarePage.bits1[s]))
            }

            cityPage.checkButton(2, !(status[8] & (1 << 7)))
            cityPage.checkButton(3, status[0] & (1 << 10))
        }
    }
}
