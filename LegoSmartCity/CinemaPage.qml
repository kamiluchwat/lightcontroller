import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: page
    width: 800
    height: 600

    property var bits: [15, 10, 11, 13, 8]
    property int clickedIndex: 0
    property bool clickedState: false

    header: Label {
        text: qsTr("Kino ")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Image {
        id: image
        anchors.left: parent.left
        anchors.right: frame2.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        source: "images/4.png"
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 0
        autoTransform: false
        fillMode: Image.PreserveAspectFit
    }

    Frame {
        id: frame2
        x: 653
        width: 125
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 0

        Column {
            id: column
            anchors.fill: parent

            Repeater{
                id: buttons
                model: ["L1", "L2", "L3", "L4", "L5"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true

                    onClicked: {
                        clickedIndex = index
                        clickedState = !clickedState
                    }
                }
            }
        }
    }

    function checkButton(index, state){
        buttons.itemAt(index).checked = state
    }

    function buttonState(index){
        return buttons.itemAt(index).checked
    }
}
