import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: page
    width: 800
    height: 600

    property var bits: [10, 8, 9, 4, 14, 5, 10, 14, 9]
    property int clickedIndex: 0
    property bool clickedState: false

    header: Label {
        text: qsTr("Miasto")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Frame {
        id: frame1
        height: 60
        anchors.left: parent.left
        anchors.right: frame2.left
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.rightMargin: 20
        anchors.leftMargin: 20

        Row {
            id: row
            anchors.fill: parent
            spacing: 5

            Repeater{
                id: buttons1
                model: ["OFF", "ON"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: false

                    onClicked: {
                        clickedIndex = index
                        clickedState = !clickedState
                    }
                }
            }

            ToolSeparator {
                id: toolSeparator
            }

            Repeater{
                id: buttons2
                model: ["Turbina", "Ulica"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: false

                    onClicked: {
                        clickedIndex = index+2
                        clickedState = !clickedState
                    }
                }
            }
        }
    }

    Frame {
        id: frame2
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.rightMargin: 20
        Column {
            id: col1
            anchors.fill: parent
            spacing: 5

            Repeater{
                id: buttons3
                model: ["L1", "L2", "L3", "L4", "L5", "L6", "L7", "L8", "L9"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true

                    onClicked: {
                        clickedIndex = index+4
                        clickedState = !clickedState
                    }
                }
            }
        }
        anchors.topMargin: 20
    }

    Image {
        id: image
        y: 252
        width: 100
        height: 100
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        source: "images/ki.jpg"
        anchors.bottomMargin: 20
        anchors.leftMargin: 20
        fillMode: Image.PreserveAspectFit
    }

    function checkButton(index, state){
        if(index < 4)
            buttons2.itemAt(index-2).checked = state
        else
            buttons3.itemAt(index-4).checked = state
    }

    function buttonState(index){
        if(index < 4)
            return buttons2.itemAt(index-2).checked
        else
            return buttons3.itemAt(index-4).checked
    }
}
