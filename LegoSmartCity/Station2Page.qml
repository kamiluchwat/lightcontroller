import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: page
    width: 800
    height: 600

    property var bits: [9, 10, 8, 14, 13, 12]
    property int clickedIndex: 0
    property bool clickedState: false

    header: Label {
        text: qsTr("Dworzec 2")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Image {
        id: image
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: frame1.top

        source: "images/12.png"
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 0
        autoTransform: false
        fillMode: Image.PreserveAspectFit
    }

    Frame {
        id: frame1
        y: 470
        height: 60
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.leftMargin: 20

        Row {
            id: row
            anchors.fill: parent
            spacing: 5

            Repeater{
                id: buttons
                model: ["L1", "L2", "L3", "L4", "L5", "L6"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true

                    onClicked: {
                        clickedIndex = index
                        clickedState = !clickedState
                    }
                }
            }
        }
    }

    function checkButton(index, state){
        buttons.itemAt(index).checked = state
    }

    function buttonState(index){
        return buttons.itemAt(index).checked
    }
}
