import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: page
    width: 800
    height: 600

    property var bits1: [7, 10, 2, 4, 13, 0, 1, 11, 12, 6, 14, 15, 3, 5]
    property var bits2: [1, 10, 0, 2, 8, 9, 12, 13, 14]
    property int clickedIndex: 0
    property bool clickedState: false

    header: Label {
        text: qsTr("Plac zgromadzeń")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Image {
        id: image
        anchors.left: frame3.right
        anchors.right: frame2.right
        anchors.top: parent.top
        anchors.bottom: frame1.top

        source: "images/2.png"
        anchors.leftMargin: 20
        anchors.rightMargin: 145
        anchors.bottomMargin: 20
        anchors.topMargin: 0
        autoTransform: false
        fillMode: Image.PreserveAspectFit
    }

    Frame {
        id: frame1
        y: 469
        height: 60
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.leftMargin: 20

        Row {
            id: row
            anchors.centerIn: parent
            spacing: 2

            Repeater{
                id: buttons1
                model: ["L1", "L2", "L3", "L4", "L5", "L6", "L7"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true

                    onClicked: {
                        clickedIndex = index
                        clickedState = !clickedState
                    }
                }
            }
        }
    }

    Frame {
        id: frame2
        x: 655
        width: 125
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: frame1.top
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 0

        Column {
            id: column
            anchors.fill: parent
            spacing: 2

            Repeater{
                id: buttons2
                model: ["L8", "L9", "L10", "L11", "L12", "L13", "L14", "L15", "L16"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true
                    anchors.horizontalCenter: parent.horizontalCenter

                    onClicked: {
                        clickedIndex = index+7
                        clickedState = !clickedState
                    }
                }
            }
        }
    }

    Frame {
        id: frame3
        width: 125
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: frame1.top
        anchors.leftMargin: 20
        Column {
            id: column1
            anchors.fill: parent
            spacing: 2

            Repeater{
                id: buttons3
                model: ["L17", "L18", "L19", "L20", "L21", "L22", "L23"]

                Button{
                    width: 100
                    height: 40
                    text: modelData
                    checkable: true
                    anchors.horizontalCenter: parent.horizontalCenter

                    onClicked: {
                        clickedIndex = index+16
                        clickedState = !clickedState
                    }
                }
            }
        }
        anchors.bottomMargin: 20
        anchors.topMargin: 0
    }

    function checkButton(index, state){
        if(index < 7)
            buttons1.itemAt(index).checked = state
        else if(index < 16)
            buttons2.itemAt(index-7).checked = state
        else
            buttons3.itemAt(index-16).checked = state
    }

    function buttonState(index){
        if(index < 7)
            return buttons1.itemAt(index).checked
        else if(index < 16)
            return buttons2.itemAt(index-7).checked
        else
            return buttons3.itemAt(index-16).checked
    }
}
